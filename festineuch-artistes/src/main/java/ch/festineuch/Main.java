package ch.festineuch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author normand.paratte
 */
public class Main {

    public static void main(String[] args) throws IOException {
        URL url = null;

        try {
            url = new URL("https://api.deezer.com/search?q=renaud");

        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        URLConnection connection = url.openConnection();

        String line;
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        JSONObject jsonObj = new JSONObject(builder.toString());

        JSONArray jsonTabArtistes = jsonObj.getJSONArray("data");

        Integer idArtiste = (Integer) jsonTabArtistes.getJSONObject(0).getJSONObject("artist").get("id");
        
        try {
            url = new URL("https://api.deezer.com/artist/"+ String.valueOf(idArtiste) +"/top");

        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        connection = url.openConnection();

        line = "";
        builder = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        jsonObj = new JSONObject(builder.toString());  
        jsonTabArtistes = jsonObj.getJSONArray("data");
        
        String topTitres[][] = {{null,null},{null,null},{null,null},{null,null},{null,null}};
        
        topTitres[0][0]=(String) jsonTabArtistes.getJSONObject(0).get("title");
        topTitres[0][1]=(String) jsonTabArtistes.getJSONObject(0).get("preview");
        topTitres[1][0]=(String) jsonTabArtistes.getJSONObject(0).get("title");
        topTitres[1][1]=(String) jsonTabArtistes.getJSONObject(0).get("preview");
        topTitres[2][0]=(String) jsonTabArtistes.getJSONObject(0).get("title");
        topTitres[2][1]=(String) jsonTabArtistes.getJSONObject(0).get("preview");
        topTitres[3][0]=(String) jsonTabArtistes.getJSONObject(0).get("title");
        topTitres[3][1]=(String) jsonTabArtistes.getJSONObject(0).get("preview");
        topTitres[4][0]=(String) jsonTabArtistes.getJSONObject(0).get("title");
        topTitres[4][1]=(String) jsonTabArtistes.getJSONObject(0).get("preview"); 
        
        System.out.println(jsonTabArtistes.getJSONObject(0).get("preview"));
    }
}
